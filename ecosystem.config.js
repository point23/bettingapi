module.exports = {
  apps : [{
    name: 'bettingapi',
    script: 'app.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    //args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'root',
      host : '80.240.25.174',
      ref  : 'origin/master',
      repo : 'https://emreyc:mPcrmewcc9z9kKmQCUbg@bitbucket.org/point23/bettingapi.git',
      path : '/var/www/bettingapi.point23.com',
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env production'
    }
  }
};
