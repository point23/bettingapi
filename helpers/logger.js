const { createLogger, format, transports } = require("winston");
const logform = require("logform");
const SlackHook = require("winston-slack-webhook-transport");

const alignedWithColorsAndTime = format.combine(
  logform.format.colorize(),
  // format.timestamp(),
  logform.format.align(),
  logform.format.printf(info => `${info.level}: ${info.message}`)
);

const logger = createLogger({
  level: process.env.LOGLEVEL,
  transports: [
    new transports.Console({
      format: alignedWithColorsAndTime
    }),
    /*new SlackHook({
      webhookUrl:
        "https://hooks.slack.com/services/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",
      channel: "botfeed",
      level: "info",
      username: "BettingBot",
      iconEmoji: ":soccer:"
    }),
    new SlackHook({
      webhookUrl:
        "https://hooks.slack.com/services/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",
      channel: "detailfeed",
      level: "debug",
      username: "BettingBot",
      iconEmoji: ":soccer:"
    })*/
  ]
});
module.exports = logger;
