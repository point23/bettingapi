const TeamSchema = require("../models/v2/team");
const MatchSchema = require("../models/v2/match");
const CompetitionSchema = require("../models/v2/competition");
const uniqueBy = require("lodash/uniqBy");
const unique = require("lodash/uniq");

/**
 * idsi verilen iki kulubu merge eder
 */
exports.mergeTeams = (team1Id, team2Id) => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await _mergeTwoTeams(team1Id, team2Id);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
};

exports.mergeTeamsMulti = (mergeToTeam, mergeFromTeams) => {
  return new Promise(async (resolve, reject) => {
    try {
      // from yoksa veya fromların icinde to varsa quit
      if (
        mergeFromTeams.length == 0 ||
        mergeFromTeams.indexOf(mergeToTeam) > -1
      ) {
        resolve(false);
        return;
      }
      for (let i = 0; i < mergeFromTeams.length; i++) {
        const mergeFromTeam = mergeFromTeams[i];
        await _mergeTwoTeams(mergeToTeam, mergeFromTeam);
      }
      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
};

function combineNames(names1, names2) {
  const newNames = unique([...names1, ...names2]);
  return newNames;
}

function combineCompetitions(team1comps, team2comps) {
  const comps = uniqueBy([...team1comps, ...team2comps], x => {
    return x.toString();
  });
  return comps;
}

function _mergeTwoTeams(teamToId, teamFromId) {
  return new Promise(async (resolve, reject) => {
    try {
      if (teamToId == teamFromId) {
        resolve(false);
        return;
      }
      const teamTo = await TeamSchema.findById(teamToId);
      const teamFrom = await TeamSchema.findById(teamFromId);

      // takım bulamazsak return
      if (!teamTo || !teamFrom) {
        resolve(false);
        return;
      }
      // team2 adını team1e ekle
      teamTo.names = combineNames(teamTo.names, teamFrom.names);
      // team2 competitionlarını team1e ekle
      teamTo.participatedCompetitions = combineCompetitions(
        teamTo.participatedCompetitions,
        teamFrom.participatedCompetitions
      );
      // update hometeams where team2 is present
      await MatchSchema.updateMany(
        {
          homeTeam: teamFrom._id
        },
        {
          homeTeam: teamTo._id
        }
      );
      // update awayTeeams where team2 is present
      await MatchSchema.updateMany(
        {
          awayTeam: teamFrom._id
        },
        {
          awayTeam: teamTo._id
        }
      );
      await teamFrom.remove();
      await teamTo.save();

      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
}

/**
 * ismi ve ülkesi verilen takımı varsa döner, yoksa yaratır döner
 * TODO kulüplerin avrupada yaptıkları maçlar için bir fix düşün
 */
exports.upsertTeam = (teamName, competition) => {
  return new Promise(async (resolve, reject) => {
    const existing = await TeamSchema.findOne({
      $or: [
        {
          name: teamName
        },
        {
          names: { $in: [teamName] }
        }
      ],
      participatedCompetitions: { $in: competition._id }
    });
    if (!existing) {
      TeamSchema.create({
        name: teamName,
        names: [teamName],
        participatedCompetitions: [competition._id]
      })
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
    } else {
      resolve(existing);
    }
  });
};

/**
 * fbd bilgisi olan competitionlari cek
 */
exports.getCompetitionsWithFBD = () => {
  return new Promise((resolve, reject) => {
    CompetitionSchema.find({
      "paths.footballdata": { $exists: true }
    })
      .then(competitions => {
        resolve(competitions);
      })
      .catch(error => {
        reject(error);
      });
  });
};

exports.getCompetitionWithFBDLeagueName = competitionFBDName => {
  return new Promise((resolve, reject) => {
    CompetitionSchema.findOne({
      "paths.footballdata": competitionFBDName
    })
      .then(competition => {
        resolve(competition);
      })
      .catch(error => {
        reject(error);
      });
  });
};
