const logger = require("../../helpers/logger");
const moment = require("moment");
const CompetitionSchema = require("../../models/v2/competition");
const lib = require("./lib");

// v1 start
exports.getCompetitions = function() {
  return new Promise(async (resolve, reject) => {
    try {
      const results = await lib.parseCompetitionList();
      for (let i = 0; i < results.length; i++) {
        const element = results[i];
        await lib.upsertCompetition(element);
      }
      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
};

exports.parseACompetition = function() {
  return new Promise(async (resolve, reject) => {
    try {
      const games = await lib.parseNextCompetition();
      for (let i = 0; i < games.length; i++) {
        const element = games[i];
        await lib.upsertGame(element);
      }
      resolve();
    } catch (error) {
      logger.error(error);
      reject(error);
    }
  });
};

exports.parseACompetitionWithId = function(competitionId) {
  return new Promise(async (resolve, reject) => {
    try {
      const games = await lib.parseCompetitionWithId(competitionId);
      for (let i = 0; i < games.length; i++) {
        const element = games[i];
        await lib.upsertGame(element);
      }
      resolve();
    } catch (error) {
      logger.error(error);
      reject(error);
    }
  });
};

exports.parseGameResults = function(dateToParse) {
  return new Promise(async (resolve, reject) => {
    logger.info("starting parsing results");
    try {
      if (dateToParse) {
        const dateObj = moment(dateToParse, "MM-DD-YYYY").toDate();
        logger.info(`starting parsing ${dateToParse}`);
        await lib.parseAndSaveGameResultsForDate(dateObj);
        logger.info(`finished parsing ${dateToParse}`);

        resolve();
      } else {
        logger.info("no date to parse, parsing today and yesterday");
        const today = new Date();
        const yesterday = moment()
          .subtract(1, "days")
          .toDate();

        logger.info("starting parsing today");
        await lib.parseAndSaveGameResultsForDate(today);
        logger.info("finished parsing today");

        logger.info("starting parsing yesterday");
        await lib.parseAndSaveGameResultsForDate(yesterday);
        logger.info("finished parsing yesterday");
        resolve();
      }
    } catch (error) {
      reject(error);
    }
  });
};

// v1 end

// v2 start
/**
 * tüm competitionları parse et
 */
exports.parseCompetitions = function() {
  return new Promise(async (resolve, reject) => {
    try {
      const competitions = await CompetitionSchema.find({
        paths: { $exists: true }
      });

      for (let i = 0; i < competitions.length; i++) {
        const c = competitions[i];
        const xm = await lib.parseACompetition(c);

        for (let i = 0; i < xm.length; i++) {
          const m = xm[i];
          await lib.saveMatch(m, c);
        }
      }

      resolve(true);
    } catch (error) {
      logger.error(error);
      reject(error);
    }
  });
};
/**
 * idsi verilen competitionı parse et
 */
exports.parseCompetition = function(competitionId) {
  return new Promise(async (resolve, reject) => {
    try {
      const competitions = await CompetitionSchema.findById(competitionId);
      let matches = [];

      const xm = await lib.parseACompetition(competitions);
      matches = [...xm, ...matches];

      for (let i = 0; i < matches.length; i++) {
        const m = matches[i];
        await lib.saveMatch(m, competitions);
      }
      resolve(matches);
    } catch (error) {
      logger.error(error);
      reject(error);
    }
  });
};

// v2 end
