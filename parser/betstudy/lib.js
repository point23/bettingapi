const axios = require("axios");
const cheerio = require("cheerio");
const logger = require("../../helpers/logger");
const moment = require("moment");
const CompetitionSchema = require("../../models/v1/competition");
const CompetitionSchemaV2 = require("../../models/v2/competition");
const GameSchema = require("../../models/v1/game");
const MatchSchema = require("../../models/v2/match");
const TeamHelper = require("../../business/teamRepository");
const cache = require("../../middlewares/cache");

// v1 start
exports.upsertCompetition = element => {
  return _upsertCompetition(element);
};

async function _upsertCompetition(element) {
  return new Promise(async (resolve, reject) => {
    try {
      let existing = await CompetitionSchema.findOne({
        path: element.path
      });
      if (!existing) {
        existing = await CompetitionSchema.create({
          name: element.name,
          path: element.path
        });
      }
      resolve(existing);
    } catch (error) {
      reject(error);
    }
  });
}

exports.parseCompetitionList = () => {
  return _parseCompetitionList();
};

async function _parseCompetitionList() {
  return new Promise(async (resolve, reject) => {
    logger.info("starting getting competitions");
    axios
      .get("https://www.betstudy.com/predictions/")
      .then(response => {
        logger.debug("downloaded html");
        const $ = cheerio.load(response.data);
        logger.debug("parsed html");
        let leagueSelect = $("select#select14");
        if (!leagueSelect || leagueSelect.length == 0) {
          logger.info("cant find competition select");
          reject("cant find competition select");
        }
        leagueSelect = leagueSelect[0];
        logger.debug("found competition dropdown");
        const result = [];
        $(leagueSelect)
          .find("option")
          .map(async (index, optionElement) => {
            if ($(optionElement).val() != "/predictions/") {
              result.push({
                name: $(optionElement).text(),
                path: $(optionElement).val()
              });
            }
          });
        logger.info(`finished competitions with ${result.length} comps`);
        resolve(result);
      })
      .catch(error => {
        logger.error(error);
        reject(error);
      });
  });
}

exports.parseAndSaveGameResultsForDate = date => {
  return _parseAndSaveGameResultsForDate(date);
};

async function _parseAndSaveGameResultsForDate(date) {
  return new Promise(async (resolve, reject) => {
    try {
      const results = await _parseGameResultsForDate(date);
      for (let i = 0; i < results.length; i++) {
        const element = results[i];
        await _saveGameResult(
          element.homeTeam,
          element.awayTeam,
          element.dateMin,
          element.dateMax,
          element.score,
          element.homeScore,
          element.awayScore
        );
      }
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

exports.parseGameResultsForDate = date => {
  return _parseGameResultsForDate(date);
};

async function _parseGameResultsForDate(date) {
  return new Promise((resolve, reject) => {
    try {
      logger.info(`starting parsing for ${date}`);
      const month = String("00" + (date.getMonth() + 1)).slice(-2);
      const day = String("00" + (date.getDate() + 0)).slice(-2);
      axios
        .get(
          `https://www.betstudy.com/matches/${date.getFullYear()}/${month}/${day}/`
        )
        .then(async response => {
          const $ = cheerio.load(response.data);
          const games = $(".match-table tbody tr");
          const result = [];

          for (let i = 0; i < games.length; i++) {
            const row = games[i];
            const tds = $(row).find("td");
            const markedAsFt = $(tds.get(0)).find("strong:not(.time)");
            if (tds.length > 0 && markedAsFt.length > 0) {
              const homeTeam = $(tds.get(1))
                .text()
                .trim();
              const awayTeam = $(tds.get(3))
                .text()
                .trim();
              const score = $(tds.get(2))
                .find("strong")
                .text()
                .trim();
              const homeScore = score.split("-")[0].trim();
              const awayScore = score.split("-")[1].trim();

              const dateMin = new Date();
              dateMin.setHours(0, 0, 0, 0);

              const dateMax = new Date();
              dateMax.setHours(0, 0, 0, 0);
              dateMax.setDate(dateMax.getDate() + 1);
              result.push({
                homeTeam,
                awayTeam,
                score,
                homeScore,
                awayScore,
                dateMin,
                dateMax
              });
            }
          }

          logger.info(`starting parsing for ${date}, resolving`);
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
    } catch (error) {
      logger.error(error);
      reject(error);
    }
  });
}

exports.saveGameResult = (
  homeTeam,
  awayTeam,
  dateMin,
  dateMax,
  score,
  homeScore,
  awayScore
) => {
  return _saveGameResult(
    homeTeam,
    awayTeam,
    dateMin,
    dateMax,
    score,
    homeScore,
    awayScore
  );
};

async function _saveGameResult(
  homeTeam,
  awayTeam,
  dateMin,
  dateMax,
  score,
  homeScore,
  awayScore
) {
  return new Promise(async (resolve, reject) => {
    try {
      logger.debug(`querying for ${homeTeam} ${awayTeam}`);
      const existingGame = await GameSchema.findOne({
        homeTeam,
        awayTeam,
        date: {
          $gte: dateMin,
          $lt: dateMax
        }
      });
      if (existingGame && existingGame.predictionSuccess == null) {
        logger.debug("found existing game, updating score");
        existingGame.score = score;
        existingGame.homeScore = homeScore;
        existingGame.awayScore = awayScore;
        existingGame.over = homeScore + awayScore >= 3;
        existingGame.under = homeScore + awayScore < 3;
        switch (existingGame.prediction) {
          case 1:
            existingGame.predictionSuccess = homeScore > awayScore;
            break;
          case 2:
            existingGame.predictionSuccess = homeScore < awayScore;
            break;
          case 3:
            existingGame.predictionSuccess = homeScore == awayScore;
            break;
          case 4:
            existingGame.predictionSuccess = existingGame.over;
            break;
          case 5:
            existingGame.predictionSuccess = existingGame.under;
            break;
        }
        await existingGame.save();
        resolve();
      } else {
        logger.debug("not found, skipping");
        resolve();
      }
    } catch (error) {
      logger.error(error);
      reject(error);
    }
  });
}

exports.parseNextCompetition = () => {
  return _parseNextCompetition();
};

async function _parseNextCompetition() {
  return new Promise(async (resolve, reject) => {
    try {
      logger.info("starting getting next competition");
      const competitionToParse = await CompetitionSchema.findOne({
        isActive: true
      }).sort("lastParseDate");
      if (competitionToParse) {
        logger.info(`competition to parse, ${competitionToParse.name}`);
        axios
          .get(`https://www.betstudy.com${competitionToParse.path}`)
          .then(async response => {
            const $ = cheerio.load(response.data);
            const table = $("table.soccer-table");
            const rows = $(table).find("tr");
            logger.debug(`found ${rows.length - 1} matches`);
            const result = [];

            rows.map(async (index, row) => {
              if ($(row).find("th").length == 0) {
                const date = new Date(
                  parseFloat(
                    $(row)
                      .find("td")
                      .first()
                      .find("span")
                      .first()
                      .attr("data-value")
                  ) * 1000
                );
                const teamTd = $($(row).find("td")[1])
                  .find("a:not(.diagram-ico)")
                  .first()
                  .text();
                const homeTeam = teamTd.split("-")[0].trim();
                const awayTeam = teamTd.split("-")[1].trim();
                const homeChance = parseInt(
                  $($(row).find("td")[2])
                    .text()
                    .replace("%", "")
                );
                const drawChance = parseInt(
                  $($(row).find("td")[3])
                    .text()
                    .replace("%", "")
                );
                const awayChance = parseInt(
                  $($(row).find("td")[4])
                    .text()
                    .replace("%", "")
                );
                const overChance = parseInt(
                  $($(row).find("td")[5])
                    .text()
                    .replace("%", "")
                );
                const underChance = parseInt(
                  $($(row).find("td")[6])
                    .text()
                    .replace("%", "")
                );
                let prediction = $($(row).find("td")[7])
                  .find("strong")
                  .first()
                  .text();
                switch (prediction) {
                  case "1":
                    prediction = 1;
                    break;
                  case "2":
                    prediction = 2;
                    break;
                  case "X":
                    prediction = 3;
                    break;
                  case "Over 2.5":
                    prediction = 4;
                    break;
                  case "Under 2.5":
                    prediction = 5;
                    break;
                }

                // takim isimleri bos gelirse kaydetmesin fix
                if (homeTeam != "" && awayTeam != "") {
                  result.push({
                    homeTeam,
                    awayTeam,
                    date,
                    homeChance,
                    drawChance,
                    awayChance,
                    overChance,
                    underChance,
                    prediction,
                    lastUpdateDate: new Date(),
                    competitionId: competitionToParse.id
                  });
                }
              }
            });

            competitionToParse.lastParseDate = new Date();
            await competitionToParse.save();
            logger.info(`finished competition`);
            resolve(result);
          });
      } else {
        logger.info("cant find competition to parse");
        resolve();
      }
    } catch (error) {
      reject(error);
    }
  });
}

exports.parseCompetitionWithId = competitionId => {
  return _parseCompetitionWithId(competitionId);
};

async function _parseCompetitionWithId(competitionId) {
  return new Promise(async (resolve, reject) => {
    try {
      logger.info("starting getting competition with id");
      const competitionToParse = await CompetitionSchema.findById(
        competitionId
      );
      if (competitionToParse) {
        logger.info(`competition to parse, ${competitionToParse.name}`);
        axios
          .get(`https://www.betstudy.com${competitionToParse.path}`)
          .then(async response => {
            const $ = cheerio.load(response.data);
            const table = $("table.soccer-table");
            const rows = $(table).find("tr");
            logger.debug(`found ${rows.length - 1} matches`);
            const result = [];

            rows.map(async (index, row) => {
              if ($(row).find("th").length == 0) {
                const date = new Date(
                  parseFloat(
                    $(row)
                      .find("td")
                      .first()
                      .find("span")
                      .first()
                      .attr("data-value")
                  ) * 1000
                );
                const teamTd = $($(row).find("td")[1])
                  .find("a:not(.diagram-ico)")
                  .first()
                  .text();
                const homeTeam = teamTd.split("-")[0].trim();
                const awayTeam = teamTd.split("-")[1].trim();
                const homeChance = parseInt(
                  $($(row).find("td")[2])
                    .text()
                    .replace("%", "")
                );
                const drawChance = parseInt(
                  $($(row).find("td")[3])
                    .text()
                    .replace("%", "")
                );
                const awayChance = parseInt(
                  $($(row).find("td")[4])
                    .text()
                    .replace("%", "")
                );
                const overChance = parseInt(
                  $($(row).find("td")[5])
                    .text()
                    .replace("%", "")
                );
                const underChance = parseInt(
                  $($(row).find("td")[6])
                    .text()
                    .replace("%", "")
                );
                let prediction = $($(row).find("td")[7])
                  .find("strong")
                  .first()
                  .text();
                switch (prediction) {
                  case "1":
                    prediction = 1;
                    break;
                  case "2":
                    prediction = 2;
                    break;
                  case "X":
                    prediction = 3;
                    break;
                  case "Over 2.5":
                    prediction = 4;
                    break;
                  case "Under 2.5":
                    prediction = 5;
                    break;
                }
                result.push({
                  homeTeam,
                  awayTeam,
                  date,
                  homeChance,
                  drawChance,
                  awayChance,
                  overChance,
                  underChance,
                  prediction,
                  lastUpdateDate: new Date(),
                  competitionId: competitionToParse.id
                });
              }
            });

            competitionToParse.lastParseDate = new Date();
            await competitionToParse.save();
            logger.info(`finished competition`);
            resolve(result);
          });
      } else {
        logger.info("cant find competition to parse");
        resolve();
      }
    } catch (error) {
      reject(error);
    }
  });
}

exports.upsertGame = gameData => {
  return _upsertGame(gameData);
};

async function _upsertGame({
  homeTeam,
  awayTeam,
  date,
  homeChance,
  drawChance,
  awayChance,
  overChance,
  underChance,
  prediction,
  lastUpdateDate,
  competitionId
}) {
  return new Promise(async (resolve, reject) => {
    try {
      const existing = await GameSchema.findOne({
        homeTeam,
        awayTeam,
        date
      });
      if (!existing) {
        logger.debug(
          `creating new game between ${homeTeam} and ${awayTeam} on ${date}`
        );
        await GameSchema.create({
          homeTeam,
          awayTeam,
          date,
          homeChance,
          drawChance,
          awayChance,
          overChance,
          underChance,
          prediction,
          lastUpdateDate,
          competitionId
        });
      } else {
        logger.debug(
          `updating game between ${homeTeam} and ${awayTeam} on ${date}`
        );
        await GameSchema.findByIdAndUpdate(existing.id, {
          homeTeam,
          awayTeam,
          date,
          homeChance,
          drawChance,
          awayChance,
          overChance,
          underChance,
          prediction,
          lastUpdateDate,
          competitionId
        });
      }
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

// v1 end

// v2 start
exports.parseCompetitionListV2 = query => {
  return _parseCompetitionListV2(query);
};
async function _parseCompetitionListV2(query = { skip: 0, take: 20 }) {
  return new Promise(async (resolve, reject) => {
    logger.info("starting getting competitions");

    let cached = cache.getWithKey("competitionList");

    if (!cached) {
      cached = [];
      const response = await axios.get("https://www.betstudy.com/predictions/");

      logger.debug("downloaded html");
      const $ = cheerio.load(response.data);
      logger.debug("parsed html");
      let leagueSelect = $("select#select14");
      if (!leagueSelect || leagueSelect.length == 0) {
        logger.info("cant find competition select");
        reject("cant find competition select");
      }
      leagueSelect = leagueSelect[0];
      logger.debug("found competition dropdown");

      $(leagueSelect)
        .find("option")
        .map(async (index, optionElement) => {
          if ($(optionElement).val() != "/predictions/") {
            if (query.name) {
              if (
                $(optionElement)
                  .text()
                  .toLocaleLowerCase()
                  .indexOf(query.name) > -1
              ) {
                cached.push({
                  id: index,
                  name: $(optionElement).text(),
                  path: $(optionElement).val()
                });
              }
            } else {
              cached.push({
                id: index,
                name: $(optionElement).text(),
                path: $(optionElement).val()
              });
            }
          }
        });

      !query.name && cache.setWithKey("competitionList", cached);

      logger.info(`finished competitions with ${cached.length} comps`);
      const resp = cached.slice(query.skip, query.skip + query.take);
      resolve({ result: resp, count: cached.length });
    } else {
      logger.info(`got competitions from cache ${cached.length} comps`);
      if (query.name) {
        const filtered = cached.filter(comp => {
          return comp.name.toLocaleLowerCase().indexOf(query.name) > -1;
        });

        const resp = filtered.slice(query.skip, query.skip + query.take);
        resolve({ result: resp, count: filtered.length });
      } else {
        const resp = cached.slice(query.skip, query.skip + query.take);
        resolve({ result: resp, count: cached.length });
      }
    }
  });
}
exports.createCompetition = body => {
  return new Promise(async (resolve, reject) => {
    let existing = await CompetitionSchemaV2.findOne({ name: body.name });
    if (!existing) {
      CompetitionSchemaV2.create({
        name: body.name,
        paths: {
          betparse: body.path
        }
      })
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
    } else {
      reject({ message: "already exists" });
    }
  });
};
exports.parseACompetition = competition => {
  return new Promise(async (resolve, reject) => {
    try {
      logger.info(`competition to parse, ${competition.name}`);
      axios
        .get(`https://www.betstudy.com${competition.paths.betparse}`)
        .then(async response => {
          const $ = cheerio.load(response.data);
          const table = $("table.soccer-table");
          const rows = $(table).find("tr");
          logger.debug(`found ${rows.length - 1} matches`);
          const result = [];

          rows.map(async (index, row) => {
            if ($(row).find("th").length == 0) {
              const date = new Date(
                parseFloat(
                  $(row)
                    .find("td")
                    .first()
                    .find("span")
                    .first()
                    .attr("data-value")
                ) * 1000
              );
              const teamTd = $($(row).find("td")[1])
                .find("a:not(.diagram-ico)")
                .first()
                .text();
              const homeTeam = teamTd.split("-")[0].trim();
              const awayTeam = teamTd.split("-")[1].trim();
              const homeChance = parseInt(
                $($(row).find("td")[2])
                  .text()
                  .replace("%", "")
              );
              const drawChance = parseInt(
                $($(row).find("td")[3])
                  .text()
                  .replace("%", "")
              );
              const awayChance = parseInt(
                $($(row).find("td")[4])
                  .text()
                  .replace("%", "")
              );
              const overChance = parseInt(
                $($(row).find("td")[5])
                  .text()
                  .replace("%", "")
              );
              const underChance = parseInt(
                $($(row).find("td")[6])
                  .text()
                  .replace("%", "")
              );
              let prediction = $($(row).find("td")[7])
                .find("strong")
                .first()
                .text();
              switch (prediction) {
                case "1":
                  prediction = 1;
                  break;
                case "2":
                  prediction = 2;
                  break;
                case "X":
                  prediction = 3;
                  break;
                case "Over 2.5":
                  prediction = 4;
                  break;
                case "Under 2.5":
                  prediction = 5;
                  break;
              }

              // takim isimleri bos gelirse kaydetmesin fix
              if (homeTeam != "" && awayTeam != "") {
                result.push({
                  homeTeam,
                  awayTeam,
                  date,
                  homeChance,
                  drawChance,
                  awayChance,
                  overChance,
                  underChance,
                  prediction,
                  lastUpdateDate: new Date(),
                  competitionId: competition.id
                });
              }
            }
          });

          logger.info(`finished competition, ${competition.name}`);
          resolve(result);
        });
    } catch (error) {
      reject(error);
    }
  });
};
exports.saveMatch = (match, competition) => {
  return new Promise(async (resolve, reject) => {
    try {
      const homeTeam = await TeamHelper.upsertTeam(match.homeTeam, competition);
      const awayTeam = await TeamHelper.upsertTeam(match.awayTeam, competition);

      const existing = await MatchSchema.findOne({
        homeTeam,
        awayTeam,
        date: match.date
      });

      if (!existing) {
        MatchSchema.create({
          homeTeam,
          awayTeam,
          date: match.date,
          predictions: {
            homeChance: match.homeChance,
            drawChance: match.drawChance,
            awayChance: match.awayChance,
            overChance: match.overChance,
            underChance: match.underChance,
            prediction: match.prediction
          },
          competition: match.competitionId
        })
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      } else {
        resolve({ message: "already exists" });
      }
    } catch (error) {
      reject({ message: "already exists" });
    }
  });
};
exports.mergeTeams = (team1, team2) => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await TeamHelper.mergeTeams(team1, team2);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
};
exports.mergeTeamsMulti = (to, from) => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await TeamHelper.mergeTeamsMulti(to, from);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
};
// v2 end
