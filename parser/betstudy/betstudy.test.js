const lib = require("../lib");
const chai = require("chai");
const should = chai.should();

describe("betstudy parser", function() {
  it("should parse todays games", function() {
    const today = new Date();
    return lib.parseGameResultsForDate(today).then(results => {
      results.should.be.a("array");
      results.should.have.property("length");
      results.length.should.be.gt(0);
    });
  });

  it("should get competition list", function() {
    return lib.parseCompetitionList().then(results => {
      results.should.be.a("array");
      results.should.have.property("length");
      results.length.should.be.gt(0);
    });
  });
});
