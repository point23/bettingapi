var schedule = require("node-schedule");
var betstudy = require("./betstudy/betstudyParser");
var footballdata = require("./footballdata/footballdata");

var jobs = [
  //v1 start
  {
    schedule: "0 0 * * *",
    callback: function() {
      betstudy.getCompetitions();
    }
  },
  {
    schedule: "*/1 * * * *",
    callback: function() {
      betstudy.parseACompetition();
    }
  },
  {
    schedule: "*/10 * * * *",
    callback: function() {
      betstudy.parseGameResults();
    }
  },
  // v1 end
  // v2 start
  {
    schedule: "0 0 * * *",
    callback: function() {
      betstudy.parseCompetitions();
    }
  },
  {
    schedule: "0 1 * * *",
    callback: function() {
      footballdata.parseGamesOfLeagues();
    }
  }
  // v2 end
];

exports.scheduleAll = function() {
  jobs.forEach(function(element) {
    element.id = schedule.scheduleJob(element.schedule, element.callback);
  }, this);
};

exports.cancelWithId = function(id) {
  jobs.forEach(function(element) {
    if (element.id == id) {
      id.cancel();
    }
  }, this);
};
