const axios = require("axios");
const csv = require("csv-parse");
const logger = require("../../helpers/logger");
const moment = require("moment");
const TeamHelper = require("../../business/teamRepository");
const MatchSchema = require("../../models/v2/match");

/**
 * tüm liglerin tüm sezonlarını parse et
 */
exports.parseSeasonsOfLeagues = () => {
  return new Promise(async (resolve, reject) => {
    try {
      logger.info("starting getting all competitions with fbd");
      const startYear = 1993;
      // fbd bilgisi olan competitionlari cek
      const competitions = await TeamHelper.getCompetitionsWithFBD();
      logger.info(`got ${competitions.length} competitions`);
      // hepsi icin 93ten 19a kadar don
      for (let i = 0; i < competitions.length; i++) {
        const competition = competitions[i];

        for (let y = startYear; y < 2019; y++) {
          const seasonString =
            (y + "").substr(2, 4) + (y + 1 + "").substr(2, 4);
          logger.info(
            `starting parsing for ${competitions.length} ${seasonString}`
          );
          // competition sene parse et
          await _parseCompetition(competition, seasonString);
        }
      }
      logger.info(`parseGamesOfLeagues finished, resolving`);
      resolve({});
    } catch (error) {
      reject(error);
    }
  });
};

/**
 * footballdata bilgisi olan tüm competitionları al ve mevcut sezonu parse et
 */
exports.parseGamesOfLeagues = () => {
  return new Promise(async (resolve, reject) => {
    try {
      logger.info("starting getting all competitions with fbd");
      const competitions = await TeamHelper.getCompetitionsWithFBD();
      logger.info(`got ${competitions.length} competitions`);
      for (let i = 0; i < competitions.length; i++) {
        const competition = competitions[i];
        await _parseCompetition(competition);
      }
      logger.info(`parseGamesOfLeagues finished, resolving`);
      resolve({});
    } catch (error) {
      reject(error);
    }
  });
};

/**
 * verilen leaguename ile competition bul
 * parse et
 */
exports.parseGamesOfLeague = (
  leagueName,
  seasonName = process.env.FBD_CURRENT_SEASON
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const competition = await TeamHelper.getCompetitionWithFBDLeagueName(
        leagueName
      );
      if (competition) {
        await _parseCompetition(competition, seasonName);
      }

      resolve({});
    } catch (error) {
      reject(error);
    }
  });
};

/**
 * verilen leaguename ile competition bul
 * historyparse et
 */
exports.parseHistoryOfLeague = (
  leagueName
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const competition = await TeamHelper.getCompetitionWithFBDLeagueName(
        leagueName
      );
      if (competition) {
        const startYear = 1993;
        for (let y = startYear; y < 2019; y++) {
          const seasonString =
            (y + "").substr(2, 4) + (y + 1 + "").substr(2, 4);

          // competition sene parse et
          await _parseCompetition(competition, seasonString);
        }
      }
      resolve({});
    } catch (error) {
      reject(error);
    }
  });
};

/**
 * competition parse et
 * @param {*} competition
 * @param {*} seasonName
 */
function _parseCompetition(
  competition,
  seasonName = process.env.FBD_CURRENT_SEASON
) {
  return new Promise(async (resolve, reject) => {
    try {
      logger.debug(`parsing ${competition.name}`);
      const url = `https://www.football-data.co.uk/mmz4281/${seasonName}/${competition.paths.footballdata}.csv`;
      const csv = await _downloadCsv(url);
      logger.debug(`downloaded csv`);
      const games = await _parseCSV(csv);
      logger.debug(`parsed csv, ${games.length} games`);

      logger.debug("starting saving matches");
      for (let i = 0; i < games.length; i++) {
        const match = games[i];
        await _saveMatch(match, competition);
      }

      logger.debug("finished saving matches, resolving");
      resolve({});
    } catch (error) {
      resolve(error);
    }
  });
}

function _downloadCsv(url) {
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function _parseCSV(data) {
  return new Promise((resolve, reject) => {
    csv(
      data,
      {
        columns: true,
        relax_column_count: true,
        skip_lines_with_empty_values: true
      },
      (err, output) => {
        if (err) {
          reject(err);
        } else {
          resolve(output);
        }
      }
    );
  });
}

function _saveMatch(match, competition) {
  return new Promise(async (resolve, reject) => {
    try {
      logger.debug(
        `starting saving match, ${match.HomeTeam} ${match.AwayTeam}`
      );
      const homeTeam = await TeamHelper.upsertTeam(match.HomeTeam, competition);
      const awayTeam = await TeamHelper.upsertTeam(match.AwayTeam, competition);
      const matchDate = moment(
        match.Date + " " + match.Time,
        "DD-MM-YYYY HH:mm"
      );

      const existing = await MatchSchema.findOne({
        homeTeam,
        awayTeam,
        date: matchDate.toDate()
      });

      if (!existing) {
        logger.debug(
          `not exists, creating ${match.HomeTeam} ${match.AwayTeam}`
        );
        MatchSchema.create({
          homeTeam,
          awayTeam,
          date: matchDate.toDate(),
          predictions: {},
          score: {
            homeScored: parseInt(match.FTHG),
            awayScored: parseInt(match.FTAG)
          },
          competition: competition._id
        })
          .then((result) => {
            resolve(result);
          })
          .catch((error) => {
            reject(error);
          });
      } else {
        if (
          !existing.score ||
          !existing.score.homeScored ||
          !existing.score.awayScored
        ) {
          logger.debug(
            `exists, updating score ${match.HomeTeam} ${match.AwayTeam}`
          );
          existing.score = {
            homeScored: parseInt(match.FTHG),
            awayScored: parseInt(match.FTAG)
          };
          await existing.save();
        }
        resolve({ message: "already exists" });
      }
    } catch (error) {
      reject(error);
    }
  });
}
