require("dotenv").config({
  path: process.env.NODE_ENV == "production" ? ".env.prod" : ".env.test"
});
var express = require("express");
var app = express();
var cors = require("cors");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var helmet = require("helmet");
var compression = require("compression");
var routes = require("./routes");
var jobs = require("./parser/jobs");
var logger = require("./helpers/logger");
var footballdata = require("./parser/footballdata/footballdata");

app.use(compression());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use("/", routes);

var port = process.env.PORT || 3000;

if (process.env.MONGOURL) {
  mongoose
    .connect(process.env.MONGOURL, { useNewUrlParser: true })
    .then(() => {
      logger.debug("Database connection successful");
    })
    .catch(err => {
      logger.debug("Database connection error");
    });
} else {
  logger.debug("Database connection string not found");
}

app.listen(port, () => {
  setTimeout(() => {
    jobs.scheduleAll();
  }, 3000);

  logger.debug(`env, ${process.env.NODE_ENV}`);
  logger.debug(`started working at, ${port}`);
});
