let mongoose = require("mongoose");
const logger = require("../../helpers/logger");

let gameSchema = new mongoose.Schema({
  homeTeam: { type: String, index: true },
  awayTeam: { type: String, index: true },
  date: { type: Date, index: true },
  homeChance: Number,
  drawChance: Number,
  awayChance: Number,
  overChance: Number,
  underChance: Number,
  prediction: Number,
  score: String,
  homeScore: Number,
  awayScore: Number,
  over: Boolean,
  under: Boolean,
  predictionSuccess: Boolean,
  lastUpdateDate: { type: Date, default: Date.now() },
  competitionId: { type: mongoose.Types.ObjectId, ref: "Competition" }
});

gameSchema.set("toJSON", { virtuals: true });

const GameSchema = mongoose.model("Game", gameSchema);
GameSchema.ensureIndexes();
GameSchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("game index done");
  }
});
module.exports = GameSchema;
