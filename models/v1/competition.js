const mongoose = require("mongoose");
const logger = require("../../helpers/logger");

const competitionSchema = new mongoose.Schema({
  name: { type: String, index: "text" },
  path: String,
  lastParseDate: { type: Date, index: true },
  isActive: { type: Boolean, default: false }
});

competitionSchema.set("toJSON", { virtuals: true });

const CompetitionSchema = mongoose.model("Competition", competitionSchema);
CompetitionSchema.ensureIndexes();
CompetitionSchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("competition index done");
  }
});
module.exports = CompetitionSchema;
