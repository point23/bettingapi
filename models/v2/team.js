const mongoose = require("mongoose");
const logger = require("../../helpers/logger");

const teamSchema = new mongoose.Schema({
  name: { type: String, index: true },
  names: { type: [String] },
  participatedCompetitions: [
    {
      type: mongoose.Types.ObjectId,
      ref: "Competition2"
    }
  ],
  logo: String
});

teamSchema.set("toJSON", { virtuals: true });

const TeamSchema = mongoose.model("Team", teamSchema);

TeamSchema.createIndexes();

TeamSchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("team index done");
  }
});
module.exports = TeamSchema;
