const mongoose = require("mongoose");
const logger = require("../../helpers/logger");

const competitionSchema = new mongoose.Schema({
  name: { type: String, index: true },
  paths: {
    betparse: String,
    oddsApi: String,
    footballdata: String
  }
});

competitionSchema.set("toJSON", { virtuals: true });

const CompetitionSchema = mongoose.model("Competition2", competitionSchema);

CompetitionSchema.createIndexes();

CompetitionSchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("competition2 index done");
  }
});
module.exports = CompetitionSchema;