const mongoose = require("mongoose");
const logger = require("../../helpers/logger");

const matchSchema = new mongoose.Schema({
  homeTeam: { type: mongoose.Types.ObjectId, ref: "Team", index: true },
  awayTeam: { type: mongoose.Types.ObjectId, ref: "Team", index: true },
  date: { type: Date, index: true },
  predictions: {
    homeChance: Number,
    drawChance: Number,
    awayChance: Number,
    overChance: Number,
    underChance: Number,
    prediction: Number,
    predictionSuccess: Boolean
  },
  odds: {},
  score: {
    homeScored: Number,
    awayScored: Number
  },
  lastUpdateDate: { type: Date, default: Date.now() },
  competition: { type: mongoose.Types.ObjectId, ref: "Competition2" }
});

matchSchema.virtual('score.displayable').get(function () {
  return this.score.homeScored + ' - ' + this.score.awayScored;
});

matchSchema.set("toJSON", { virtuals: true });

const MatchSchema = mongoose.model("Match", matchSchema);

MatchSchema.createIndexes();

MatchSchema.on("index", (err) => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("match index done");
  }
});
module.exports = MatchSchema;
