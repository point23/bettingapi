const express = require("express");
const router = express.Router();
const CompetitionSchema = require("../models/v2/competition");
const MatchSchema = require("../models/v2/match");
const TeamSchema = require("../models/v2/team");
const buildRestController = require("../controllers/restController");
const adminController = require("../controllers/adminController");

router.use((req, res, next) => {
  req.query.take = req.query.take ? parseInt(req.query.take) : 10;
  req.query.skip = req.query.skip ? parseInt(req.query.skip) : 0;
  if (req.query.sort && req.query.by && req.query.by === "DESC") {
    req.query.sort = "-" + req.query.sort;
  }
  next();
});

const restRoutes = [
  {
    schema: CompetitionSchema,
    path: "/competitions"
  },
  {
    schema: MatchSchema,
    path: "/matches"
  },
  {
    schema: TeamSchema,
    path: "/teams"
  }
];

function bindRest(router, schema, path) {
  const controller = buildRestController(schema);
  router
    .route(path)
    .get(controller.list)
    .post(controller.create);
  router
    .route(path + "/:id")
    .get(controller.detail)
    .put(controller.update)
    .patch(controller.patch)
    .delete(controller.remove);
}

restRoutes.map(route => {
  bindRest(router, route.schema, route.path);
});

// admin routes start
router
  .route("/betparseCompetitions")
  .post(adminController.createCompetitionFromBetparse)
  .get(adminController.getBetStudyCompetitions);
router
  .route("/betparseCompetitions/parse")
  .post(adminController.forceParseBetStudyCompetition);
router
  .route("/betparseCompetitions/parse/all")
  .post(adminController.forceParseBetStudyCompetitions);
router
  .route("/footballdata/parse")
  .post(adminController.footballDataParseHistoryOfLeague);
router
  .route("/footballdata/parse/all")
  .post(adminController.footballDataParseCurrentSeason);
router
  .route("/footballdata/parse/all/forever")
  .post(adminController.footballDataParseEveryting);
router.route("/teams/merge").post(adminController.mergeTeams);
router.route("/teams/merge/multi").post(adminController.mergeTeamsMulti);
// admin routes end

module.exports = router;
