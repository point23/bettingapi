const express = require("express");
const router = express.Router();
const restRouter = require("./restRoutes");
const v1 = require("./v1");
const v2 = require("./v2");

router.use("/", v1);
router.use("/rest", restRouter);

router.use("/v2", v2);

module.exports = router;