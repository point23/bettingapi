const express = require("express");
const router = express.Router();
const cache = require("../middlewares/cache");
const apiController = require("../controllers/apiController");

function responseHandler(req, res, next){
  res.json(res.locals.data);
}

router.use((req, res, next)=>{
  req.query.take = req.query.take ? parseInt(req.query.take) : 20;
  req.query.skip = req.query.skip ? parseInt(req.query.skip) : 0;
  next();
})

router.get(
  "/",
  cache.get,
  //processQuery,
  apiController.findGamesV2,
  cache.set,
  responseHandler
);

router.get(
  "/today",
  cache.get,
  //processQuery,
  apiController.todayV2,
  cache.set,
  responseHandler
);

router.post(
  "/pastGames",
  apiController.pastGamesV2,
  responseHandler
);

router.get(
  "/competitions",
  apiController.competitionListV2,
  responseHandler
);

module.exports = router;