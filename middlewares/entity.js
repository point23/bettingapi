exports.getAllEntities = function(schema, request) {
  return new Promise((resolve, reject) => {
    const query = prepareQuery(request);
    schema
      .find(query)
      .sort(request.sort)
      .skip(request.skip)
      .limit(request.take)
      .then(entities => {
        resolve(entities);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.createEntity = function(schema, data) {
  return new Promise((resolve, reject) => {
    schema
      .create(data)
      .then(newEntity => {
        resolve(newEntity);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.getEntityWithId = function(schema, id) {
  return new Promise((resolve, reject) => {
    schema
      .findById(id)
      .then(entity => {
        resolve(entity);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.updateEntity = function(schema, id, entity) {
  return new Promise((resolve, reject) => {
    schema
      .findByIdAndUpdate(id, entity, {
        new: true
      })
      .then(entity2 => {
        resolve(entity2);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.deleteEntity = function(schema, id) {
  return new Promise((resolve, reject) => {
    schema
      .findByIdAndRemove(id)
      .then(entity => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.patchEntity = function(schema, id, request) {
  return new Promise((resolve, reject) => {
    schema
      .findById(id)
      .then(result => {
        delete request._id;
        for (const property in request) {
          result[property] = request[property];
        }
        return result.save();
      })
      .then(result => {
        resolve(result);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.findOneEntity = function(schema, query) {
  return new Promise((resolve, reject) => {
    schema
      .findOne(query)
      .then(result => {
        resolve(result);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.count = function(schema, request) {
  return new Promise((resolve, reject) => {
    const query = prepareQuery(request);
    schema
      .countDocuments(query)
      .then(count => {
        resolve(count);
      })
      .catch(err => {
        reject(err);
      });
  });
};

function prepareQuery(request) {
  const query = {};
  const skipQuery = ["skip", "take", "limit", "page", "sort", "by", "range"];
  const textSearchable = ["name"];

  for (const key in request) {
    if (request.hasOwnProperty(key)) {
      const element = request[key];
      if (!skipQuery.includes(key)) {
        if (Array.isArray(element)) {
          query[key] = {
            $in: element
          };
        } else {
          if (textSearchable.includes(key)) {
            /*query["$text"] = {
              $search: element
            };*/

            query[key] = new RegExp(element, "ig");
          } else {
            query[key] = element;
          }
        }
      }
    }
  }

  return query;
}
