// middlewares/cache.js

const NodeCache = require("node-cache");

// stdTTL: time to live in seconds for every generated cache element.
const cache = new NodeCache({ stdTTL: 5 * 60 });

function getUrlFromRequest(req) {
  const url = req.protocol + "://" + req.headers.host + req.originalUrl;
  return url;
}

function set(req, res, next) {
  const url = getUrlFromRequest(req);
  cache.set(url, res.locals.data);
  return next();
}

function setWithKey(key, data) {
  cache.set(key, data);
}

function get(req, res, next) {
  const url = getUrlFromRequest(req);
  const content = cache.get(url);
  if (content) {
    res.locals.data = content;
    //return res.status(200).send(content);
  }
  return next();
}

function getWithKey(key) {
  const content = cache.get(key);
  return content;
}

module.exports = { get, set, getWithKey, setWithKey };
