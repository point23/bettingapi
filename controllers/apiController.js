/**
 * bu dosyada uygulamaların kullandigi servisler bulunur
 */

const GameSchema = require("../models/v1/game");
const MatchSchema = require("../models/v2/match");
const CompetitionSchema = require("../models/v2/competition");
const _ = require("lodash");

function prepareQuery(request) {
  const query = {};
  const skipQuery = ["skip", "take", "limit", "page", "sort", "by"];

  for (const key in request) {
    if (request.hasOwnProperty(key)) {
      const element = request[key];
      if (!skipQuery.includes(key)) {
        if (Array.isArray(element)) {
          query[key] = {
            $in: element
          };
        } else {
          query[key] = element;
        }
      }
    }
  }

  return query;
}

// v1 start
exports.findGames = function(req, res, next) {
  const query = prepareQuery(req.query);
  query.date = { $gt: new Date() };
  GameSchema.where(query)
    .sort("date")
    .skip(req.query.skip)
    .limit(req.query.take)
    .then(games => {
      res.locals.data = games;
      next();
    })
    .catch(() => {
      res.locals.data = [];
      next();
    });
};

exports.getCompetitions = function(req, res, next) {
  GameSchema.find({
    date: { $gte: new Date() }
  })
    .populate("competitionId")
    .then(allGames => {
      const allComps = allGames.map(game => {
        return game.competitionId;
      });
      const result = _.sortBy(_.uniqBy(allComps, "_id"), "name");
      res.locals.data = result;
      next();
    })
    .catch(() => {
      res.locals.data = [];
      next();
    });
  /*CompetitionSchema.find({})
    .then(competitions => {
      res.locals.data = competitions;
      next();
    })
    .catch(error => {
      res.locals.data = [];
      next();
    });*/
};

exports.today = function(req, res, next) {
  const query = prepareQuery(req.query);

  const dateMin = new Date();
  dateMin.setHours(0, 0, 0, 0);

  const dateMax = new Date();
  dateMax.setHours(0, 0, 0, 0);
  dateMax.setDate(dateMax.getDate() + 1);

  query.date = { $gt: dateMin, $lte: dateMax };
  GameSchema.where(query)
    .sort("date")
    .skip(req.query.skip)
    .limit(req.query.take)
    .then(games => {
      res.locals.data = games;
      next();
    })
    .catch(() => {
      res.locals.data = [];
      next();
    });
};
// v1 end

// v2 start
exports.findGamesV2 = function(req, res, next) {
  const query = prepareQuery(req.query);
  query.date = { $gt: new Date() };
  MatchSchema.where(query)
    .sort("date")
    .skip(req.query.skip)
    .limit(req.query.take)
    .populate(["homeTeam", "awayTeam", "competition"])
    .then(games => {
      res.locals.data = games;
      next();
    })
    .catch(() => {
      res.locals.data = [];
      next();
    });
};
exports.todayV2 = function(req, res, next) {
  const query = prepareQuery(req.query);

  const dateMin = new Date();
  dateMin.setHours(0, 0, 0, 0);

  const dateMax = new Date();
  dateMax.setHours(0, 0, 0, 0);
  dateMax.setDate(dateMax.getDate() + 1);

  query.date = { $gt: dateMin, $lte: dateMax };
  MatchSchema.where(query)
    .sort("date")
    .skip(req.query.skip)
    .limit(req.query.take)
    .populate(["homeTeam", "awayTeam", "competition"])
    .then(games => {
      res.locals.data = games;
      next();
    })
    .catch(() => {
      res.locals.data = [];
      next();
    });
};
exports.pastGamesV2 = function(req, res, next) {
  const dateMin = new Date();
  dateMin.setHours(0, 0, 0, 0);

  MatchSchema.find({
    date: { $lt: dateMin },
    homeTeam: { $in: [req.body.homeTeam, req.body.awayTeam] },
    awayTeam: { $in: [req.body.homeTeam, req.body.awayTeam] }
  })
    .sort("-date")
    .populate(["homeTeam", "awayTeam", "competition"])
    .then(games => {
      res.locals.data = games;
      next();
    })
    .catch(() => {
      res.locals.data = [];
      next();
    });
};
exports.competitionListV2 = function(req, res, next) {
  CompetitionSchema.find({})
    .sort("name")
    .then(comps => {
      res.locals.data = comps;
      next();
    })
    .catch(() => {
      res.locals.data = [];
      next();
    });
};
// v2 end
