const betstudyParser = require("../parser/betstudy/betstudyParser");
const lib = require("../parser/betstudy/lib");
const footballdata = require("../parser/footballdata/footballdata");

// v1 start
exports.forceParseCompetitionList = function(req, res) {
  betstudyParser
    .getCompetitions()
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};

exports.forceParseGameResults = function(req, res) {
  betstudyParser
    .parseGameResults(req.body.dateToParse)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};

exports.forceParseGamesOfCompetition = function(req, res) {
  betstudyParser
    .parseACompetitionWithId(req.body.competitionId)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};
// v1 end

// v2 start
exports.forceParseBetStudyCompetition = function(req, res) {
  betstudyParser
    .parseCompetition(req.body.competitionId)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};

exports.forceParseBetStudyCompetitions = function(req, res) {
  betstudyParser
    .parseCompetitions()
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};

exports.getBetStudyCompetitions = function(req, res) {
  lib
    .parseCompetitionListV2(req.query)
    .then(response => {
      res.setHeader("Access-Control-Expose-Headers", "x-total-count");
      res.setHeader("X-Total-Count", response.count);
      res.json(response.result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};

exports.createCompetitionFromBetparse = function(req, res) {
  lib
    .createCompetition(req.body)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};
exports.mergeTeams = function(req, res) {
  lib
    .mergeTeams(req.body.team1, req.body.team2)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};
exports.mergeTeamsMulti = function(req, res) {
  lib
    .mergeTeamsMulti(req.body.to, req.body.from)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};
exports.footballDataParseCurrentSeason = function(req, res) {
  footballdata
    .parseGamesOfLeagues()
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};
exports.footballDataParseEveryting = function(req, res) {
  footballdata
    .parseSeasonsOfLeagues()
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};
exports.footballDataParseHistoryOfLeague = function(req, res) {
  footballdata
    .parseHistoryOfLeague(req.body.leagueName)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.status(500).send({ message: error.message });
    });
};
// v2 end
