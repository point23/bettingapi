/**
 * Bu dosyada admin panelin kullandigi servisler bulunur
 */

const EntityActions = require("../middlewares/entity");

const buildRestController = function(schemax) {
  const schema = schemax;
  function list(req, res) {
    EntityActions.getAllEntities(schema, req.query)
      .then(async ds => {
        const count = await EntityActions.count(schema, req.query);
        res.setHeader("Access-Control-Expose-Headers", "x-total-count");
        res.setHeader("X-Total-Count", count);
        res.json(ds);
      })
      .catch(error => {
        res.status(500).send({ message: error.message });
      });
  }
  function create(req, res) {
    EntityActions.createEntity(schema, req.body)
      .then(ds => {
        res.json(ds);
      })
      .catch(error => {
        res.status(500).send({ message: error.message });
      });
  }
  function detail(req, res) {
    EntityActions.getEntityWithId(schema, req.params.id)
      .then((ds) => {
        res.json(ds);
      })
      .catch((error) => {
        res.status(500).send({ message: error.message });
      });
  }
  function update(req, res) {
    EntityActions.updateEntity(schema, req.params.id, req.body)
      .then((ds) => {
        res.json(ds);
      })
      .catch((error) => {
        res.status(500).send({ message: error.message });
      });
  }
  function patch(req, res) {
    EntityActions.patchEntity(schema, req.params.id, req.body)
      .then((ds) => {
        res.json(ds);
      })
      .catch((error) => {
        res.status(500).send({ message: error.message });
      });
  }
  function remove(req, res) {
    EntityActions.deleteEntity(schema, req.params.id)
      .then((ds) => {
        res.json(ds);
      })
      .catch((error) => {
        res.status(500).send({ message: error.message });
      });
  }

  return {
    list,
    create,
    detail,
    update,
    patch,
    remove
  };
};

module.exports = buildRestController;
